Use db2twitter
==============
After the configuration of db2twitter, just launch the following command::

    $ db2twitter /path/to/db2twitter.ini

To use the circle mode, meaning you will alternatively tweet X tweets of your Y last tweets of your database (see the :ref:`configure-section-label` section), use the --circle option:

    $ db2twitter --circle /path/to/db2twitter.ini

We recommend using db2twitter with cron. The following line in /etc/crontab will check for new db rows in your database every minute, build and send tweets accordingly::

    # m h dom mon dow user  command
    * * * * * db2twitter db2twitter /path/to/db2twitter.ini
    0 * * * * db2twitter db2twitter --circle /path/to/db2twitter.ini
