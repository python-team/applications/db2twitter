#!/usr/bin/env python3.4
# -*- coding: utf-8 -*-
# Copyright © 2015 Carl Chenet <chaica@backupcheckerproject.org>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Unit tests for db2twitter
'''Unit tests for db2twitter'''

import datetime
import sys
import unittest

from db2twitter.cliparse import CliParse
from db2twitter.confparse import ConfParse
from db2twitter.twbuild import TwBuild
from db2twitter.timetosend import TimeToSend

class TestDb2Twitter(unittest.TestCase):
    '''TestDb2Twitter class'''

    def test_getconfigfile(self):
        '''Test the CliParse class'''
        filepath = './tests.py'
        circling = False
        sys.argv[-1] = filepath
        clip = CliParse()
        self.assertEqual(clip.args, {'validpathtoconf': filepath, 'iscircling': circling})

    def test_getconfigvars(self):
        '''Test the ConfParse class'''
        confp = ConfParse('tests/getconfigvars/db2twitter.ini')
        self.assertEqual(confp.confvalues, {'consumer_key': 'qPsJvBZlE2yatbsLM4dQL1r3m',
                                            'consumer_secret': 'ixM1AvyzVJxjKmsQtmQzJ8pmB4vlhmYIO5v2XqEl0mqIdIwie5',
                                            'access_token': '2370158707-m8eN2YcfVcKGfWmVLERDGMd1UPKHVPzAQHoP7qN',
                                            'access_token_secret': 'tB74LfWy551sRTU49buGE6YXIukY74TmBK6JLECeOA5ti',
                                            'tweet': '{} recrute un {} https://www.linuxjobs.fr/jobs/{}',
                                            'hashtags': 'devops,linux,',
                                            'upper_first_char': True,
                                            'dbconnector': 'mysql+mysqlconnector',
                                            'dbhost': 'localhost',
                                            'database': 'linuxjobs',
                                            'dbuser': 'linuxjobs',
                                            'dbpass': 'xxxxxxxxx',
                                            'dbtables': 'jobs,',
                                            'ids': {},
                                            'rows': {'jobs': ['company_name','title','id']},
                                            'sqlitepath': '/var/lib/db2twitter/db2twitter.db',
                                            'sqlfilter': {},
                                            'days': 'mon-fri,',
                                            'hours': '0-11,14-17,',
                                            'circlelasttwnb': '3',
                                            'circletwbatchnb': '2',
                                            'sqlfilter': {},
                                            })

    def test_gettwbuild(self):
        '''Test the TwBuild class'''
        conf= { 'tweet': '{} recrute un {} https://www.linuxjobs.fr/jobs/{}',
                'hashtags': 'devops,linux,',
                'upper_first_char': True,}
        twbuildp = TwBuild(conf, [['machin', 'devops', 1]])
        self.assertEqual(twbuildp.readytotweet, ['Machin recrute un #devops https://www.linuxjobs.fr/jobs/1'])

    def test_timetosendhourstrue(self):
        '''Test the TimeToSend class - return True'''
        conf = { 'days': 'mon-sun,',
                #'hours': '0,1,2,3,4,5-7,8,9,10,11,12,13,14,15,16,17,18,19-23,',}
                'hours': '0-23,',}
        tts = TimeToSend(conf)
        self.assertTrue(tts.sendthetweet)

    def test_timetosendhourslisttrue(self):
        '''Test the TimeToSend class with a list of hours- return True'''
        conf = { 'days': 'mon-sun,',
                'hours': '0-23,',}
        tts = TimeToSend(conf)
        self.assertTrue(tts.sendthetweet)

    @unittest.skipIf(((datetime.datetime.now().weekday() == 6) and (datetime.datetime.now().hour == 0)), 'only executing if we are not sunday and not midnight')
    def test_timetosendhoursfalse(self):
        '''Test the TimeToSend class - return False'''
        conf = { 'days': 'sun,',
                'hours': '0,',}
        tts = TimeToSend(conf)
        self.assertFalse(tts.sendthetweet)

################################################################
#
# End of the unit tests
#
################################################################

if __name__ == '__main__':
    unittest.main()
