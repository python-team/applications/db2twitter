Documentation for the db2twitter project
========================================

db2twitter automatically extracts fields from your database, use them to feed a template of
tweet and send the tweet.
From 0.2 db2twitter stores already sent tweets in a sqlite3 database.

You'll find below anything you need to install, configure or run db2twitter.

Guide
=====

.. toctree::
   :maxdepth: 2

   install
   configure
   use
   license
   authors


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

